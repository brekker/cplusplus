#include <iostream>
#include <string>

using namespace std;

class Item
{
private:
    string secondmane;
    int salary;
    int male;
    string male_str;

public:
    Item()
    {
    }

    Item(string secondmane, int salary, int male, string male_str)
    {
        this->secondmane = secondmane;
        this->salary = salary;
        this->male = male;
        this->male_str = male_str;
    }

    Item(Item* item)
    {
        this->secondmane = item->GetSecondmane();
        this->salary = item->GetSalary();
        this->male = item->GetMale();
        this->male_str = item->GetMaleSTR();
    }

    void SetSecondmane(string secondmane)
    {
        this->secondmane = secondmane;
    }

    string GetSecondmane()
    {
        return secondmane;
    }

    void SetSalary(int salary)
    {
        this->salary = salary;
    }

    int GetSalary()
    {
        return salary;
    }

    void SetMale(int male)
    {
        this->male = male;
    }

    int GetMale()
    {
        return male;
    }

    void SetMaleSTR(string male)
    {
        this->male_str = male_str;
    }

    string GetMaleSTR()
    {
        return male_str;
    }
};

//bool operator < (Item left, Item rigth)
//{
//    if (left.GetSalary() != rigth.GetSalary())
//    {
//        return left.GetSalary() < rigth.GetSalary();
//    }
//    return left.GetSecondmane() < rigth.GetSecondmane();
//}

bool operator < (Item left, Item rigth)
{
    return left.GetSalary() < rigth.GetSalary();
}

bool operator > (Item left, Item rigth)
{
    return left.GetSalary() > rigth.GetSalary();
}