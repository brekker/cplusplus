//1. Read and save from text files
//2. Use heads file
//3. Override operators

#include <iostream>
#include <fstream>
#include <string>
#include "Item.h";

using namespace std;

int main()
{
    fstream fin, fout;
    fin.open("input.txt", fstream::in);
    fout.open("output.txt", fstream::out);

    int n;
    fin >> n;
    Item* items = new Item[n];

    for (int i = 0; i < n; i++)
    {
        string secondmane;
        int salary;
        int male;
        string male_str;
        
        getline(fin >> ws, secondmane);
        fin >> salary >> male;
        fin.ignore();
        getline(fin, male_str);

        Item item = Item(secondmane, salary, male, male_str);
        items[i] = item;
    }

    for (size_t i = 0; i < n-1; i++)
    {
        for (size_t j = 0; j < n-i-1; j++)
        {
            if (items[j] > items[j + 1]) 
            {
                swap(items[j], items[j + 1]);
            }
        }
    }

    for (size_t i = 0; i < n; i++)
    {
        fout << items[i].GetSecondmane() << endl << items[i].GetSalary() << endl << items[i].GetMale() << endl << items[i].GetMaleSTR() << endl;
    }

    fin.close();
    fout.close();
    std::cout << "\nDone!";
}
