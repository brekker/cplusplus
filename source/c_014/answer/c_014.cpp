#include <iostream>
#include <string>
#include <fstream>
//remove words with first char 'b' or 'd' and count them

using namespace std;

int main()
{
    fstream fin;
    fin.open("input.txt", fstream::in);
    fstream fout;
    fout.open("output.txt", fstream::out);

    int count = 0;
    while (!fin.eof())
    {
        string input;
        getline(fin, input);
        if (input[0] == 'b' || input[0] == 'd')
        {
            count++;
        }
        else
        {
            fout << input << "\n";
        }
    }

    fout << "\ncount: " << count;
    fout.close();
    fin.close();
    std::cout << "Done!\n";
}
