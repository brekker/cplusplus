#include <iostream>
#include <string>

using namespace std;

void split(string input)
{
    int delimeterPosition = input.find(",");
    while (delimeterPosition > -1)
    {
        string word = input.substr(0, delimeterPosition);
        cout << word << '\n';
        //input = input.substr(delimeterPosition+1);
        input = input.erase(0, delimeterPosition+1);
        delimeterPosition = input.find(",");
    }
    cout << input << '\n';
}

void splitAndConcat(string input, int position)
{
    string result = "";
    int delimeterPosition = input.find(" ");
    while (delimeterPosition > -1)
    {
        string word = input.substr(0, delimeterPosition);
        cout << word << '\n';
        cout << word[position] << '\n';
        result += word[position];
        //input = input.substr(delimeterPosition+1);
        input = input.erase(0, delimeterPosition + 1);
        delimeterPosition = input.find(" ");
    }
    cout << input << '\n';
    cout << input[position] << '\n';
    result += input[position];
    cout << "result:" << result << '\n';
}

void deleteBrackets(string input, string open, string close)
{
    int leftPosition = input.find(open);
    int rigthPosition = input.find(close);
    while (leftPosition > -1)
    {
        input.erase(leftPosition, rigthPosition - leftPosition + close.length());
        leftPosition = input.find(open);
        rigthPosition = input.find(close);
    }
    cout << input << "\n";
}

string replaceSubtring(string input, string old, string new_)
{
    int index = input.find(old);
    while (index > -1) {
        input = input.replace(index, old.length(), new_);
        index = input.find(old);
    }
    return input;
}

int main()
{
    string input = "";
    while (true)
    {
        cout << "Enter text:";
        getline(cin, input);
        input = replaceSubtring(input, "(", "[");
        input = replaceSubtring(input, ")", "]");
        cout << input;
        //deleteBrackets(input, "[[", "]]");
    }
    return 0;
}