#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <ctime>
using namespace std;

const int n = 25;

void PrintCell(int** state, int i, int j)
{
    if (state[i][j] == 2) 
    {
        cout << "OO";
        return;
    }
    cout << "  ";
}

void PrintState(int** state)
{
    //cout << "\x1B[2J\x1B[H";
    system("cls");
    for (size_t i = 0; i < n+1; i++)
    {
        cout << "OO";
    }
    cout << endl;
    for (size_t i = 0; i < n; i++)
    {
        cout << "O";
        for (size_t j = 0; j < n; j++)
        {
            PrintCell(state, i, j);
        }
        cout << "O" << endl;
    }
    for (size_t i = 0; i < n + 1; i++)
    {
        cout << "OO";
    }
    cout << endl;
}

void InitState(int ** state)
{
    state = new int*[n];
    for (size_t i = 0; i < n; i++)
    {
        state[i] = new int [n];
    }
    for (size_t i = 0; i < n; i++)
    {
        for (size_t j = 0; j < n; j++)
        {
            state[i][j] = 0;
        }
    }
}

void KillEat(int** state)
{
    for (size_t i = 0; i < n; i++)
    {
        for (size_t j = 0; j < n; j++)
        {
            if (state[i][j] == 2)
            {
                state[i][j] = 0;
                return;
            }
        }
    }
}

void RandomEat(int** state)
{
    KillEat(state);
    int i = (rand() % n);
    int j = (rand() % n);
    state[i][j] = 2;
}

int main()
{
    srand((unsigned)time(0));
    int** state = new int*[n];
    for (size_t i = 0; i < n; i++)
    {
        state[i] = new int [n];
    }
    char key;
    while (true)
    {
        PrintState(state);
        RandomEat(state);
        //Sleep(1000);
        cin >> key;
    }
    return 0;
}

/*
    //// Output prompt
    //cout << "Press any key to continue..." << endl;

    //// Set terminal to raw mode
    //system("stty raw");

    //// Wait for single character
    //char input = getchar();

    //// Echo input:
    //cout << "--" << input << "--";

    //// Reset terminal to normal "cooked" mode
    //system("stty cooked");

    //// And we're out of here
*/