//remove words with first char 'b' or 'd' and count them, swap words 0 and 2
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

string* split(string s, int& n) {
    string* result = new string[s.length()];
    int index = s.find(" ");
    while (index > -1) {
        result[n++] = s.substr(0, index);
        s.erase(0, index + 1);
        index = s.find(" ");
    }
    if (s.length() > 0) {
        result[n++] = s.substr(0, index);
    }

    return result;
}


int main()
{
    fstream fin;
    fin.open("input.txt", fstream::in);

    string input;
    getline(fin, input);


    int n = 0;
    string* words = split(input, n);

    fstream fout;
    fout.open("output.txt", fstream::out);

    string temp = words[2];
    words[2] = words[0];
    words[0] = temp;

    int count = 0;
    for (size_t i = 0; i < n; i++)
    {
        if (words[i][0] == 'b' || words[i][0] == 'd')
        {
            count++;
        }
        else
        {
            fout << words[i] << " ";
        }
    }

    fout << "\ncount: " << count;
    fout.close();
    fin.close();
    std::cout << "Done!\n";
}

