#include <iostream>
#include <string>
using namespace std;

class Item
{
private:
    string title;
    int age;
    float price;

public:
    Item()
    {
    }

    Item(string title1)
    {
        this->title = title1;
        this->age = 1;
        this->price = 100;
    }

    Item(string title, int age, float price)
    {
        this->title = title;
        this->age = age;
        this->price = price;
    }

    void SetTitle(string title)
    {
        this->title = title;
    }

    string GetTitle()
    {
        return title;
    }

    void SetAge(int age)
    {
        this->age = age;
    }

    int GetAge()
    {
        return age;
    }

    void SetPrice(float price)
    {
        this->price = price;
    }

    float GetPrice()
    {
        return price;
    }

    void Print()
    {
        cout << "Title: " << title << ". Age: " << age << ". Price: " << price << endl;
    }

    static void ReadItem(Item& item)
    {
        cin.ignore();
        getline(cin, item.title);
        cin >> item.age;
        cin >> item.price;
    }

    float ShippingPrice()
    {
        return 100 - 0.5 * this->age;
    }

    static float USD_To_EUR(float priceInUSD)
    {
        return priceInUSD * 0.976;
    }

    static void PrintLogo()
    {
        cout << "Logo will be here!";
    }
};

int main()
{
    cout << (true ? "true" : "false") << endl;
    return 0;
    int n;
    cin >> n;
    Item* items = new Item[n];
    for (size_t i = 0; i < n; i++)
    {
        Item::ReadItem(items[i]);
    }
    for (size_t i = 0; i < n; i++)
    {
        items[i].Print();
    }

    cout << "\nPrice more then 100:\n";
    for (size_t i = 0; i < n; i++)
    {
        if (items[i].GetPrice() > 100)
        {
            items[i].Print();
        }
    }

    int targetIndex = 0;
    for (size_t i = 0; i < n; i++)
    {
        if (items[i].GetAge() >= 14)
        {
            targetIndex = i;
            break;
        }
    }

    for (size_t i = targetIndex + 1; i < n; i++)
    {
        if (items[i].GetAge() >= 14 && items[i].GetPrice() > items[targetIndex].GetPrice())
        {
            targetIndex = i;
        }
    }
    cout << "\nMax price from age more then 14:\n";
    items[targetIndex].Print();

    for (size_t i = 0; i < n; i++)
    {
        for (size_t j = 0; j < n - i - 1; j++)
        {
            if (items[j].GetAge() > items[j + 1].GetAge())
            {
                Item temp = items[j + 1];
                items[j + 1] = items[j];
                items[j] = temp;
            }
        }
    }
    cout << "\nSorted:\n";
    for (size_t i = 0; i < n; i++)
    {
        items[i].Print();
    }
}